# -*- coding: utf-8 -*-
from django.shortcuts import render
import json
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return render(request, 'index.html')

@csrf_exempt
def login(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        if data['login'] == 'test' and data['password'] == 'test':
            response = {'access': True}
        else:
            response = {'access': False}
        return JsonResponse(response)
    else:
        return HttpResponse(u'������ ������ ������������ ����� POST');