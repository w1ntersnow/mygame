$(function() {

    $(document).on("click", "#canvas-main", function(event){
        // ФИксируем  координаты клика
        var x = event.pageX-$(this).offset().left;
        var y = event.pageY-$(this).offset().top;
        // Готовим запрос к серверу. Запрос содержит координаты полигона и точки куда был произведен клик
        var query = {
            "polygon": polygon,
            "point": [x, y]
        };
        // Получаем доступ к холсту
        var context = $(this)[0].getContext("2d");

        // Выполняем POST запрос к серверу
        $.ajax({
            type: "POST",
            url: '/api/in_polygon',
            data: JSON.stringify(query),
            success: function(data) {
                // ОБрабатываем полученный ответ
                p = data['point'];

                // Рисуем круг в точке клика
                context.beginPath();
                context.arc(p[0], p[1], 5, 0, 2 * Math.PI, false);

                // По результату запроса заливаем круг зеленым или красным цветом
                if (data['in_polygon'])
                    context.fillStyle = "#73AD21";
                else
                    context.fillStyle = "#FF0000";

                context.fill();
            }
        });
    });
});