$(function() {
var attempt = 3; // Variable to count number of attempts.
var session = null;
// Below function Executes on click of login button.
if (session === null) {
    $( "#dialog" ).dialog();
}
$(document.querySelector('form > input[id="submit"]')).on("click", function(event) {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var query = {
        'login': username,
        'password': password
    };
    $.ajax({
        type: "POST",
        url: 'login',
        data: JSON.stringify(query),
        success: function(data) {
            if ( data['access'] === true){
                alert ("Login successfully");
                document.querySelector('div [id="dialog"]').innerHTML  = 'Logged as ' +username
                session = data['login']
                return false;
            }
            else{
                attempt --;// Decrementing by one.
                alert("You have left "+attempt+" attempt;");
                // Disabling fields after 3 attempts.
                if( attempt == 0){
                    document.getElementById("username").disabled = true;
                    document.getElementById("password").disabled = true;
                    document.getElementById("submit").disabled = true;
                    return false;
                }
            }
        }
    });

})
});

